alias install='sudo apt-get install'
alias update='sudo apt-get update'
alias upgrade='sudo apt-get upgrade'
alias upgrayed='update && upgrade'
alias v='vim'
alias ..='cd ../;ls'
alias ...='cd ../../;ls'
alias ....='cd ../../../;ls'
alias .....='cd ../../../../;ls'
alias ......='cd ../../../../../;ls'
alias reload='bash'
alias e='exit'
alias grepr='grep -in -r'
alias master='git push -u origin master'
alias com='git commit -m'
alias add='git add --all'

function cd {
   builtin cd "$@" && ls -F
}


#computer specific 
alias home='ssh hootis@96.57.35.74 -p 6969'
alias rut='cd /home/hootis/Documents/rustcoin/rcoin/src/'
alias base='cd /home/hootis/Documents/rustcoin/'
alias crypt='cd /home/hootis/Documents/rustcoin/rust-crypto/src/'
alias cstar=' cd ~/projects/cstar/'

alias lowest='sudo su -c "echo 1 > /sys/class/backlight/amdgpu_bl0/brightness"'
alias low='sudo su -c "echo 25 > /sys/class/backlight/amdgpu_bl0/brightness"'
alias med='sudo su -c "echo 75 > /sys/class/backlight/amdgpu_bl0/brightness"'
alias high='sudo su -c "echo 175 > /sys/class/backlight/amdgpu_bl0/brightness"'
alias highest='sudo su -c "echo 255 > /sys/class/backlight/amdgpu_bl0/brightness"'
