set nocompatible
filetype off

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Plugin 'VundleVim/Vundle.vim'
Plugin 'Valloric/YouCompleteMe'
Plugin 'rdnetto/YCM-Generator'
Plugin 'vim-scripts/SearchComplete'
Plugin 'scrooloose/nerdtree'
"get tabpular
Plugin 'majutsushi/tagbar'
Plugin 'vim-scripts/a.vim'
Plugin 'nanotech/Jellybeans.vim'
Plugin 'rust-lang/rust.vim'

call vundle#end()
filetype plugin indent on 
set tabstop=3
set shiftwidth=3
set foldnestmax=1
set foldmethod=marker
set foldmarker={,}
set pastetoggle=<F2>

map q zi
map t zo
map f zc
map e $
map <F7> :tabn<cr>
map <F8> :tabp<cr>
map <F4> :NERDTree<cr>
map <F3> :w!<cr>:A<cr>
nmap <F6> :TagbarToggle<CR><C-w>w
nmap <F5> <C-w>w

set number

set exrc
set secure

"Being custom delete
"These lines make ,d cut and d just delete
"I mainly use Yank
"They also replace the leader key to ,
nnoremap x "_x
nnoremap d "_d
nnoremap D "_D
vnoremap d "_d
nnoremap <leader>d ""d
nnoremap <leader>D ""D
vnoremap <leader>d ""d

let mapleader = ","
let g:mapleader = ","
"End custom delete

"You complete me 
"close scratch window after completion
let g:ycm_autoclose_preview_window_after_completion = 1
let g:ycm_autoclose_preview_window_after_insertion = 1
let g:ycm_confirm_extra_conf = 0
let g:ycm_rust_src_path = '/home/hootis/Documents/rust/src'

"Colour Scheme

colorscheme jellybeans
function! WordFrequency() range
	let all = split(join(getline(a:firstline, a:lastline)), '\A\+')
	let frequencies = {}
	for word in all
		let frequencies[word] = get(frequencies, word, 0) + 1
	endfor
	new
	setlocal buftype=nofile bufhidden=hide noswapfile tabstop=20
	for [key,value] in items(frequencies)
		call apppend('$', key."\t".value)
	endfor
	sort i 
endfunction

"command~ -range=% WordFrequency <line1>,<line2>call WordFrequency()

if !exists(":DiffOrig")
	command DiffOrig vert new | set bt=nofile | r # | 0d_ | diffthis
				\ | wincmd p |diffthis
endif
"End jellybeans
set expandtab
