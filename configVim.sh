#!/bin/bash
sudo apt update
sudo apt install build-essential cmake
sudo apt install python-dev python3-dev
cd $HOME
mkdir .vim
cd .vim
mkdir bundle
cd $HOME
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim

vim +PluginInstall +qall
cd $HOME/.vim/bundle/YouCompleteMe/
./install.py --racer-completer --clang-completer


