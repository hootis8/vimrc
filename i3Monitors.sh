#!/bin/bash

xrandr --output DisplayPort-3 --auto --rotate left --above eDP
xrandr --output DisplayPort-2 --auto --rotate left --right-of DisplayPort-3

